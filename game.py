from random import randint
import string
print("Hi! What is your name?")

name = input("Answer:")

for guess_number in range(1,6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print("Guess :", guess_number, "Were you born in",
            month_number, "/", year_number, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Goodbye Human.")
    else: 
        print("Drat! Lemme try again!")

